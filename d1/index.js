// console.log("Hello World!");

// [SECTION] Arithmetic Operators

    let x = 1397;
    let y = 7831;

    // Addition
    let sum = x + y;
    console.log("Result of addition operator: " + sum);

    // Subtraction
    let difference = x - y;
    console.log("Result of subtraction operator: " + difference);

    // Multiplication
    let product = x * y;
    console.log("Result of multiplication operator: " + product);

    // Division

    let quotient = x / y;
    console.log("Result of division operator: " + quotient);

    // Modulo -> remainder: it can use to know if value is even or odd
    let modulo = x % y;
    console.log("Result of modulo operator: " + modulo);

    
    
    // [SECTION] Assignment Operator

        // Basic Assignment operator (=)
        // The assignment operator assigns the value of the "right hand" operand to a variable.

        let assignmentNumber = 8;
        console.log("The current value of the assignment variable: " + assignmentNumber);

    // Addition Assignment Operator
            // 8+2
    // assignment = assignment + 2; long method
    //assignmentNumber += 2; // shorthand method
    assignmentNumber = assignmentNumber + 2;
    console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

    // Subtraction/Multiplication/Division( -=, *=, /=)

    assignmentNumber -= 2;
    console.log("Result of subtraction assignmentNumber operator: " +assignmentNumber);

    assignmentNumber *= 2;
    console.log("Result of multiplication assignmentNumber operator: " +assignmentNumber);

    assignmentNumber /= 2;
    console.log("Result of division assignmentNumber operator: " +assignmentNumber);


//  [SECTION] PEMDAS (Order of Operations)
//  Multiple Operators and Parenthesis
//  When multiple operators are applied in a single statement, it follows PEMDAS.


/*  MDAS
    The operation were done in the following orders
    1. 3 * 4 = 12 | 1 + 2 - 12 / 5
    2. 12 / 5 = 2.4 | 1 + 2 - 2.4
    3. 1 + 2 = 3 | 3 - 2.4
    4. 3 - 2.4 = 0.6 // result
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " +mdas);


/* PEMDAS
    The operation were done in the following order:
    1. 4 / 5 = 0.8 | 1 + (2 - 3) * 0.8
    2. 2 - 3 = -1 | 1 + (-1) * 0.8
    3. -1 * 0.8 = -0.8
    4. 1 - 0.8 = 0.20 // result 
*/
// let pemdas = 1 + (2 -3) * (4/5); // .20
// grouping the operators using parenthesis will deliver different result.
let pemdas = (1 + (2 -3)) * (4/5);
console.log("Result of pemdas operation: "+pemdas); // 0


// [SECTION] Increment and Decrement 
// Operators that add or subtract a values by 1 and reassign the value of the variable where the increment(++)/decrement (--) was applied.

let z = 1;
    // 1 + 1
let increment = ++z;
console.log("Result of pre-increment: " +increment);
console.log("Result of pre-in for z: " +z);

    // 2 javscript read the code from top to bot
increment = z++;
console.log("Result of post-increment: " +increment)
console.log("Result of the post increment for z: "+z);



let decrement = --z;
console.log("Result of pre-decrement: " +decrement);
console.log("Result of pre-decrement for z: "+z);

decrement = z--;
console.log("Result of post-decrement: " +decrement);
console.log("Result of post-decrement for z: "+z);


// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another.

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); // 30
console.log(typeof nonCoercion);


/* 
    - The result is a number
    - The boolean "true" is also associated with the value of 1.
*/
let numE = true + 1;
console.log(numE);
/* 
    - The result is a number
    - The boolean "false" is also associated with the value of 0.
*/
let numF = false + 1;
console.log(numF);


// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands.
// After evaluation, it returns a boolean value
// https://www.w3schools.com/js/js_comparisons.asp

let juan = "juan";

// Equality Operator (==)
/* 
    - Checks whether the operands are equal/have the same content.
*/

console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == 'one'); // false
console.log(0 == false); // true
// compare two strings that are the same.
console.log('juan' == 'juan'); // true
// Compares a string with the variable juan
console.log('juan' == juan);

// Inequality Operator (!=)
/* 
    - checks whether the operands are not equal/have different value.
    - Attempts to CONVERT and COMPARE operand of dif data type.
*/

console.log(1 !=1 ); // false
console.log(1 !=2 ); // true
console.log(0 != false); // false
console.log('juan' != 'juan'); // false
console.log('juan' != juan); // false


// Strict Equality Operator (===)
/* 
    Check whether the operands are equal/have the same content.
    - Also COMPARES the DATA TYPES of 2 values.
*/

console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); // false
console.log(0 === false); // false
console.log('juan' === 'juan'); // true
console.log('juan' === juan); // true


// Strict Inequality Operator (!==)
/* 
    - Checks whether the operands are not equal/dont have the same content.
    - Also COMPARES the DATA TYPES of 2 values
*/
console.log(1 !== 1); // false
console.log(1 !== 2); // true
console.log(1 !== '1'); // true
console.log(0 !== false); // true
console.log('juan' !=='juan'); // false
console.log('juan' !==juan); // false


// [SECTION] Relational Operators
/* 
    Some comparison operators check wether one value is greater or less than to other value.
*/

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)

let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan); 

// LT or Less Than Operator (<)
let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan); 

// GTE or Greater Than or Equal (>=)
let isGTOrEqual = a >=b; // 50 >= 65 = false
console.log(isGTOrEqual);

// LTE or Less Than or Equal (<=)
let isLEOrEqual = a <= b; // 50 <= 65 = true
console.log(isLEOrEqual);

let numStr = "30";
console.log(a > numStr); // true - force coercion to change the string to number.

let str = "twenty";
console.log(b>=str); //false
// Since the string is not numeric, the string was not converted to a number. 65 >= NaN (Not a Number); 


// [SECTION] Logical Operators
/* 
    Logical operator allows us to be more specific in the logical combination of conditions and evaluations. It return boolean.
*/

let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&& - Double Apmersand)
// Returns true if all operands are true.
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double Pipe)
// Returns true if one of the operand are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " +someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge;
console.log("Result of logical NOT Operator: " +someRequirementsNotMet);